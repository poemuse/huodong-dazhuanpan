package com.bluemerkava.fireblue.p3.wechat.dazhuanpan.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bluemerkava.fireblue.p3.wechat.common.controller.base.BaseController;
import com.bluemerkava.fireblue.p3.wechat.common.util.AppUtil;
import com.bluemerkava.fireblue.p3.wechat.common.util.Const;
import com.bluemerkava.fireblue.p3.wechat.common.util.DelAllFile;
import com.bluemerkava.fireblue.p3.wechat.common.util.Freemarker;
import com.bluemerkava.fireblue.p3.wechat.common.util.PageData;
import com.bluemerkava.fireblue.p3.wechat.common.util.PathUtil;
import com.bluemerkava.fireblue.p3.wechat.common.util.Tools;
import com.bluemerkava.fireblue.p3.wechat.common.util.ViewFreemarker;
import com.bluemerkava.fireblue.p3.wechat.dazhuanpan.entity.HuodongEntity;
import com.bluemerkava.fireblue.p3.wechat.dazhuanpan.service.HudongManager;
import com.bluemerkava.fireblue.p3.wechat.dazhuanpan.service.impl.HudongService;
import com.bluemerkava.fireblue.p3.wechat.dazhuanpan.util.LogUtil;

/**
 * 类名称： 页面静态化 创建人：FH Q313596790 修改时间：2016年12月27日
 * 
 * @version
 */
@Controller
@RequestMapping(value = "/p3/createHtml")
public class P3CreateHtmlController extends BaseController {

	@Resource
	HudongManager hudongService;

	// @Resource(name = "aboutusService")
	// private AboutusManager aboutusService;

	/**
	 * 生成关于我们页面
	 * 
	 * @param response
	 * @throws Exception
	 */
	//@ResponseBody
	@RequestMapping(value = "/proDazhuanpan")
	public void proAboutus(HttpServletResponse response) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> root = new HashMap<String, Object>(); // 创建数据模型
		// this.getInfo(root); // 网站基本信息
		// this.getImg(root, "4"); // 技术团队
		// PageData pdC = new PageData();
		// pdC.put("ABOUTUS_ID", "21cb9db019d34444b0255da591125e21");
		// pdC = aboutusService.findById(pdC);
		// root.put("CONTENT", pdC.getString("CONTENT")); // 关于我们
		// root.put("POWER", pdC.getString("POWER")); // 技术实力
		// PC端
		DelAllFile.delFolder(PathUtil.getClasspath() + "webdazhuanpan.html"); // 生成代码前,先清空之前生成的文件
		// Freemarker.printFile("dazhuanpan.ftl", root, "webdazhuanpan.html", "",
		// getFtlPath());
		// 手机端
		/*
		 * DelAllFile.delFolder(PathUtil.getClasspath() + "webaboutusm.html"); //
		 * 生成代码前,先清空之前生成的文件 Freemarker.printFile("mobile/aboutusTemplate.ftl", root,
		 * "webaboutusm.html", "", getFtlPath());
		 */
		PageData pdC = this.getPageData();
		pdC.put("TYPE", "2");
		pdC.put("ACCOUNTID", "402881e8461795c201461795c2e90000");
		// 获取当前公众帐号的大转盘规则
		pdC = hudongService.findOne(pdC);
		// request.setAttribute("hdId", pdC.getString("ID"));
		// request.setAttribute("openId", pdC.getString("OPENID"));
		// request.setAttribute("accountid", pdC.getString("ACCOUNTID"));

		map.put("hdId", pdC.getString("ID"));
		//map.put("openId", pdC.getString("OPENID"));
		map.put("openId", "123456");
		map.put("accountid", pdC.getString("ACCOUNTID"));
		HuodongEntity hdEntity = new HuodongEntity();
		hdEntity.setPriceone(pdC.getString("PRICEONE"));
		hdEntity.setOnetotal(Integer.parseInt(pdC.get("ONETOTAL").toString()));
		hdEntity.setPricetwo(pdC.getString("PRICETWO"));
		hdEntity.setTwototal(Integer.parseInt(pdC.get("TWOTOTAL").toString()));
		hdEntity.setPricethree(pdC.getString("PRICETHREE"));
		hdEntity.setThreetotal(Integer.parseInt(pdC.get("THREETOTAL").toString()));
		hdEntity.setDescription(pdC.getString("DESCRIPTION"));
		map.put("hdEntity", hdEntity);
		String templateName = "p3/dazhuanpan/ftl/zhuanpan.ftl";
		ViewFreemarker.view(response, templateName, map);

		Map<String, Object> remap = new HashMap<String, Object>();
		remap.put("result", "success");
		//return AppUtil.returnObject(new PageData(), remap);

	}

	/**
	 * 大转盘
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(params = "goZhuanpan")
	public ModelAndView goZhuanpan(HttpServletRequest request) throws Exception {
		PageData pdC = this.getPageData();
		String openId = pdC.getString("openId");
		String accountid = request.getParameter("accountid");

		pdC.put("TYPE", "2");
		pdC.put("ACCOUNTID", "402881e8461795c201461795c2e90000");
		// 获取当前公众帐号的大转盘规则
		pdC = hudongService.findOne(pdC);
		HuodongEntity hdEntity = new HuodongEntity();
		// 显示前台的活动规则
		// request.setAttribute("hdEntity", hdEntity);
		request.setAttribute("hdId", pdC.getString("ID"));
		request.setAttribute("openId", pdC.getString("OPENID"));
		request.setAttribute("accountid", pdC.getString("ACCOUNTID"));
		LogUtil.info("....hdid...." + hdEntity.getId() + "...openId.." + openId);
		return new ModelAndView("weixin/idea/huodong/zp/zhuanpan");
	}

	/**
	 * 获取路径
	 */
	public String getFtlPath() {
		// return Tools.readTxtFile(Const.FTLPATH);
		return Const.FTLPATH;
	}

}
