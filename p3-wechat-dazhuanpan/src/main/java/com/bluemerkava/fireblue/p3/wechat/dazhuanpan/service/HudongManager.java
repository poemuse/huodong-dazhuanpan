package com.bluemerkava.fireblue.p3.wechat.dazhuanpan.service;

import java.util.List;

import com.bluemerkava.fireblue.p3.wechat.common.entity.Page;
import com.bluemerkava.fireblue.p3.wechat.common.util.PageData;

/** 
 * 说明： 互动接口
 * 创建人：FH Q313596790
 * 创建时间：2018-09-04
 * @version
 */
public interface HudongManager{

	/**新增
	 * @param pd
	 * @throws Exception
	 */
	public void save(PageData pd)throws Exception;
	
	/**删除
	 * @param pd
	 * @throws Exception
	 */
	public void delete(PageData pd)throws Exception;
	
	/**修改
	 * @param pd
	 * @throws Exception
	 */
	public void edit(PageData pd)throws Exception;
	
	/**列表
	 * @param page
	 * @throws Exception
	 */
	public List<PageData> list(Page page)throws Exception;
	
	/**列表(全部)
	 * @param pd
	 * @throws Exception
	 */
	public List<PageData> listAll(PageData pd)throws Exception;
	
	/**通过id获取数据
	 * @param pd
	 * @throws Exception
	 */
	public PageData findById(PageData pd)throws Exception;
	
	/**
	 * 获取一条数据
	 * @param pd
	 * @return
	 * @throws Exception
	 */
	public PageData findOne(PageData pd)throws Exception;
	
	/**批量删除
	 * @param ArrayDATA_IDS
	 * @throws Exception
	 */
	public void deleteAll(String[] ArrayDATA_IDS)throws Exception;
	
}

