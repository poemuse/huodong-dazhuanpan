package com.bluemerkava.fireblue.p3.wechat.dazhuanpan.service.impl;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

import com.bluemerkava.fireblue.common.DaoSupport;
import com.bluemerkava.fireblue.p3.wechat.common.entity.Page;
import com.bluemerkava.fireblue.p3.wechat.common.util.PageData;
import com.bluemerkava.fireblue.p3.wechat.dazhuanpan.service.HudongManager;

/**
 * 说明： 互动 创建人：FH Q313596790 创建时间：2018-09-04
 * 
 * @version
 */
@Service("hudongService")
public class HudongService implements HudongManager {

	@Resource(name = "daoSupport")
	private DaoSupport dao;

	/**
	 * 新增
	 * 
	 * @param pd
	 * @throws Exception
	 */
	public void save(PageData pd) throws Exception {
		dao.save("HudongMapper.save", pd);
	}

	/**
	 * 删除
	 * 
	 * @param pd
	 * @throws Exception
	 */
	public void delete(PageData pd) throws Exception {
		dao.delete("HudongMapper.delete", pd);
	}

	/**
	 * 修改
	 * 
	 * @param pd
	 * @throws Exception
	 */
	public void edit(PageData pd) throws Exception {
		dao.update("HudongMapper.edit", pd);
	}

	/**
	 * 列表
	 * 
	 * @param page
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<PageData> list(Page page) throws Exception {
		return (List<PageData>) dao.findForList("HudongMapper.datalistPage", page);
	}

	/**
	 * 列表(全部)
	 * 
	 * @param pd
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<PageData> listAll(PageData pd) throws Exception {
		return (List<PageData>) dao.findForList("HudongMapper.listAll", pd);
	}

	/**
	 * 通过id获取数据
	 * 
	 * @param pd
	 * @throws Exception
	 */
	public PageData findById(PageData pd) throws Exception {
		return (PageData) dao.findForObject("HudongMapper.findById", pd);
	}

	@Override
	public PageData findOne(PageData pd) throws Exception {
		return (PageData) dao.findForObject("HudongMapper.findOne", pd);
	}

	/**
	 * 批量删除
	 * 
	 * @param ArrayDATA_IDS
	 * @throws Exception
	 */
	public void deleteAll(String[] ArrayDATA_IDS) throws Exception {
		dao.delete("HudongMapper.deleteAll", ArrayDATA_IDS);
	}

}
